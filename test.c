#include "btreep/btreep.h"

typedef struct DATA{

	int key;
	char data[30];
}DATA;

unsigned char cmp_data(DATA* a, DATA* b){

	return a->key < b->key;
}

void* return_key(DATA* a){

	return &a->key;
}

void print_data(DATA* data, unsigned int offset){

	for(unsigned int i=0; i < offset; i++) printf(" ");
	printf("%d\n", data->key);
	for(unsigned int i=0; i < offset; i++) printf(" ");
	printf("%s", data->data);
}

void print_key(int* key, unsigned int offset){

	for(unsigned int i=0; i < offset; i++) printf(" ");
	printf("%d", *key);
}

int main(){

	BTREEP* btreep = btreep_init("btreep_test", 3, sizeof(int), sizeof(DATA));

	btreep->cmp = (unsigned char(*)(void*, void*))cmp_data;
	btreep->key = (void*(*)(void*))return_key;
	btreep->print_data = (void(*)(void*, unsigned int))print_data;
	btreep->print_key = (void(*)(void*, unsigned int))print_key;

	DATA datas[] = {
		{1, "test1"},
		{13,"key"},
		{5, "plz n"},
		{11, "jujubas"},
		{709, "horaja"},
		{32, "assasdsa"},
		{1312, "BREAK"},
		{-1, "antimatter"},
		{6, "hello 2"},
		{9, "aahah"},
		{7, "as"},
		{8, "free corruption"},
		{10, "yeah"},
		{42, "hey"},
		{31, "yeasda"},
		{21, "lets force a merge"},
		{43, "alsdadsadasdasdas"},
		{344, "sl"},
		{41, "oi"}
	};

	for(unsigned int i = 0; i < sizeof(datas)/sizeof(datas[0]); i++){

		btreep_insert(btreep, &datas[i]);
		printf("after insert:\n");
		btreep_print(btreep);
	}

	int key;

	do{

		printf("input key to remove: ");
		scanf(" %d", &key);

		btreep_remove(btreep, &key);

		printf("after removal: \n");
		btreep_print(btreep);

	}while(key);

	btreep_struct_save(btreep, "btreep_test");

	btreep_free(btreep);

	return 0;
}
