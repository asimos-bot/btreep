# Btreep

Implementation of a B tree + data structure. It uses `void*` pointer to ensure it can work with any data type.

## Features

All operations are immediatly saved to files in the filename given to `btreep_init`.

Since `void*` pointers are used, functions for comparing data, printing keys, returning a key and
printing data need to be given to the `BTREEP` struct.
