CC = gcc

OPT = -g

LINK = btreep/*/*.o

BTREEP = btreep/*/*.c

MAIN = test.c

all:
	$(CC) $(OPT) $(LINK) $(BTREEP) $(MAIN) -o test
