#ifndef BTREEP_NODE_H
#define BTREEP_NODE_H

/*
	BNODE FILE STRUCTURE:

	  1 byte      8 bytes     4 bytes
	|----------------------------------|
	| is_leaf | parent_file | num_keys |
	|----------------------------------|
*/

typedef struct BNODE{

	//id of this node
	unsigned long id;

	//1 if leaf, 0 otherwise
	unsigned char is_leaf;

	//number of keys in this node
	unsigned int num_keys;

	//id of the parent
	unsigned long parent_file;

	//file pointer to this node in secondary memory
	FILE* file;

	//enough space for leaf and index attributes
	unsigned char offset[16];

}BNODE;

//create node struct with new id, and a file for it
BNODE* bnode_create(BTREEP*, unsigned char is_leaf, unsigned int num_keys, unsigned long parent_file);

//save bnode to its file
void bnode_save(BTREEP*, BNODE*);

//load bnode struct from file
BNODE* bnode_load(BTREEP*, unsigned long id);

//return index of the element with the key given (return -1 on error)
unsigned int bnode_get_key_index(BTREEP*, BNODE*, void* key);

//return key/data at given index (return NULL on error, like out of bounds and others)
void* bnode_get_key_at(BTREEP*, BNODE*, unsigned int index);

//set value of key at given index (return 1 if operation was sucessful)
unsigned char bnode_set_key_at(BTREEP*, BNODE*, unsigned int index, void* key);

//remove key at given index and returns it (return NULL on error)
void* bnode_remove_key_at(BTREEP*, BNODE*, unsigned int index);

//insert key in the right position, returns its index
unsigned int bnode_insert_key(BTREEP*, BNODE*, void* key);

/*
	QUICK READINGS
*/

unsigned char bnode_get_is_leaf(unsigned long id);

unsigned int bnode_get_num_keys(unsigned long id);

unsigned char bnode_can_insert(unsigned long id, unsigned short t);

unsigned char bnode_can_remove(unsigned long id, unsigned short t);

unsigned long bnode_get_parent_file(unsigned long id);

void* bnode_get_key(BTREEP*, unsigned long id, unsigned int index);

/*
	QUICK WRITINGS
*/

void bnode_set_parent_file(unsigned long id, unsigned long parent_file);

//return right sibling, return 0 if none
unsigned long bnode_get_right_sibling(BTREEP* btreep, BNODE* bnode);

//return left sibling, return 0 if none
unsigned long bnode_get_left_sibling(BTREEP* btreep, BNODE* bnode);

#endif
