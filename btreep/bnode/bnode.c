#include "../btreep.h"

//create node struct with new id, and a file for it
BNODE* bnode_create(BTREEP* btreep, unsigned char is_leaf, unsigned int num_keys, unsigned long parent_file){

	//if any argument is invalid return NULL
	if( !btreep ) return NULL;

	//allocate memory for bnode struct
	BNODE* bnode = malloc(sizeof(BNODE));

	//generate new valid id
	bnode->id = btreep_struct_generate_id(btreep);

	bnode->num_keys = num_keys;
	bnode->parent_file = parent_file;
	bnode->is_leaf = is_leaf;

	//make filename
	char filename[30];
	sprintf(filename, "btreep_data/%u.dat", bnode->id);

	//create file for this node
	bnode->file = fopen(filename, "w+b");

	//write is_leaf
	fwrite(&bnode->is_leaf, sizeof(unsigned char), 1, bnode->file);

	//write parent_file
	fwrite(&bnode->parent_file, sizeof(unsigned long), 1, bnode->file);

	//write number of keys
	fwrite(&bnode->num_keys, sizeof(unsigned int), 1, bnode->file);

	return bnode;
}

//save bnode to its file
void bnode_save(BTREEP* btreep, BNODE* bnode){

	if( !btreep || !bnode ) return;

	//set cursor to the beginning of the file
	fseek(bnode->file, 0, SEEK_SET);

	//write is_leaf
	fwrite(&bnode->is_leaf, sizeof(unsigned char), 1, bnode->file);

	//write parent_file
	fwrite(&bnode->parent_file, sizeof(unsigned long), 1, bnode->file);

	//write number of keys
	fwrite(&bnode->num_keys, sizeof(unsigned int), 1, bnode->file);
}

//load bnode struct from file
BNODE* bnode_load(BTREEP* btreep, unsigned long id){

	if( !btreep || !id ) return NULL;

	//allocate memory for bnode struct
	BNODE* bnode = malloc(sizeof(BNODE));

	//set id
	bnode->id = id;

	//make filename string
	char filename[30];
	sprintf(filename, "btreep_data/%u.dat", id);

	//open file, return NULL on failure
	if( !( bnode->file = fopen(filename, "r+b") ) ) return NULL;

	//read is_leaf status
	fread(&bnode->is_leaf, sizeof(unsigned char), 1, bnode->file);

	//read parent_file
	fread(&bnode->parent_file, sizeof(unsigned long), 1, bnode->file);

	//read number of keys
	fread(&bnode->num_keys, sizeof(unsigned int), 1, bnode->file);

	return bnode;
}

//return index of the element with the key given
unsigned int bnode_get_key_index(BTREEP* btreep, BNODE* bnode, void* key){

	return bnode->is_leaf ? bleaf_get_key_index(btreep, (BLEAF*)bnode, key) :
	bindex_get_key_index(btreep, (BINDEX*)bnode, key);
}

//return key/data at given index (return NULL on error, like out of bounds and others)
void* bnode_get_key_at(BTREEP* btreep, BNODE* bnode, unsigned int index){

	return bnode->is_leaf ? bleaf_get_key_at(btreep, (BLEAF*)bnode, index) :
	bindex_get_key_at(btreep, (BINDEX*)bnode, index);
}

//set value of key at given index (return 1 if operation was sucessful)
unsigned char bnode_set_key_at(BTREEP* btreep, BNODE* bnode, unsigned int index, void* key){

	return bnode->is_leaf ? bleaf_set_data_at(btreep, (BLEAF*)bnode, index, key) :
	bindex_set_key_at(btreep, (BINDEX*)bnode, index, key);
}

//remove key at given index and returns it (return NULL on error)
void* bnode_remove_key_at(BTREEP* btreep, BNODE* bnode, unsigned int index){

	return bnode->is_leaf ? bleaf_remove_data_at(btreep, (BLEAF*)bnode, index) :
	bindex_remove_key_at(btreep, (BINDEX*)bnode, index);
}

//insert key in the right position, returns its index
unsigned int bnode_insert_key(BTREEP* btreep, BNODE* bnode, void* key){

	return bnode->is_leaf ? bleaf_insert_data(btreep, (BLEAF*)bnode, key) :
	bindex_insert_key(btreep, (BINDEX*)bnode, key);
}

unsigned char bnode_get_is_leaf(unsigned long id){

	if( !id ) return 0;

	FILE* file;

	//get file
	char filename[30];
	sprintf(filename, "btreep_data/%u.dat", id);

	if( !( file = fopen(filename, "rb")) ) return 0;

	//see if its leaf
	unsigned char is_leaf;
	fread(&is_leaf, sizeof(unsigned char), 1, file);

	fclose(file);
	return is_leaf;
}

unsigned int bnode_get_num_keys(unsigned long id){

	if( !id ) return 0;
	
	FILE* file;

	//get file
	char filename[30];
	sprintf(filename, "btreep_data/%u.dat", id);

	if( !( file = fopen(filename, "rb")) ) return 0;

	unsigned int num_keys;

	//go to num_keys field location
	fseek(file, 9, SEEK_SET);

	//get num keys
	fread(&num_keys, sizeof(unsigned int), 1, file);

	fclose(file);

	return num_keys;
}

unsigned char bnode_can_insert(unsigned long id, unsigned short t){

	if( t<2 ) return 0;

	return bnode_get_num_keys(id) < (t<<1) - 1;
}

unsigned char bnode_can_remove(unsigned long id, unsigned short t){

	if( t < 2 ) return 0;

	return bnode_get_num_keys(id) > t - 1;
}

unsigned long bnode_get_parent_file(unsigned long id){

	if( !id ) return 0;
	
	FILE* file;

	//get file
	char filename[30];
	sprintf(filename, "btreep_data/%u.dat", id);

	if( !( file = fopen(filename, "rb")) ) return 0;
	
	unsigned long parent_file;

	//go to num_keys field location
	fseek(file, 1, SEEK_SET);

	//get num keys
	fread(&parent_file, sizeof(unsigned long), 1, file);

	fclose(file);
	return parent_file;
}

void* bnode_get_key(BTREEP* btreep, unsigned long id, unsigned int index){

	if( !id || index >= bnode_get_num_keys(id) ) return NULL;

	FILE* file;
	void* key;

	//get file
	char filename[30];
	sprintf(filename, "btreep_data/%u.dat", id);

	if( !( file = fopen(filename, "rb")) ) return 0;

	if( bnode_get_is_leaf(id) ){

		//get dummy leaf struct
		BLEAF bleaf;
		bleaf.file = file;
		bleaf.num_keys = bnode_get_num_keys(id);

		//trick this function to give us the key from the file
		key = bleaf_get_key_at(btreep, &bleaf, index);

		fclose(file);
		return key;
	}else{

		//move cursor to proper position
		fseek(file, 21 + (sizeof(unsigned long) + btreep->key_len)*index, SEEK_SET);

		//allocate memory for key
		void* key = malloc(btreep->key_len);

		//get key
		fread(key, btreep->key_len, 1, file);

		fclose(file);
		return key;
	}
}

void bnode_set_parent_file(unsigned long id, unsigned long parent_file){

	if( !id ) return;

	FILE* file;

	//get file
	char filename[30];
	sprintf(filename, "btreep_data/%u.dat", id);

	if( !( file = fopen(filename, "r+b")) ) return;

	//go to parent_file field
	fseek(file, 1, SEEK_SET);

	//overwrite parent file
	fwrite(&parent_file, sizeof(unsigned long), 1, file);

	//close file (never forget plz)
	fclose(file);
}

//return right sibling, return 0 if none
unsigned long bnode_get_right_sibling(BTREEP* btreep, BNODE* bnode){

	if( !btreep || !bnode || !bnode->parent_file ) return 0;

	//get parent node
	BINDEX* parent = bindex_load(btreep, bnode->parent_file);

	unsigned int bnode_interval_index = bindex_get_interval_index(btreep, parent, bnode->id);

	unsigned int num_keys = parent->num_keys;

	if( bnode_interval_index > num_keys-1 ){

		bindex_free(btreep, parent);
		return 0;
	}

	unsigned long interval = parent->intervals[bnode_interval_index+1];

	bindex_free(btreep, parent);

	return interval;
}

//return left sibling, return 0 if none
unsigned long bnode_get_left_sibling(BTREEP* btreep, BNODE* bnode){

	if( !btreep || !bnode || !bnode->parent_file ) return 0;
	
	//get parent node
	BINDEX* parent = bindex_load(btreep, bnode->parent_file);

	unsigned int bnode_interval_index = bindex_get_interval_index(btreep, parent, bnode->id);

	unsigned int num_keys = parent->num_keys;

	if( bnode_interval_index > num_keys || !bnode_interval_index ){

		bindex_free(btreep, parent);
		return 0;
	}

	unsigned long interval = parent->intervals[bnode_interval_index-1];

	bindex_free(btreep, parent);

	return interval;
}


