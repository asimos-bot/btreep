#include "../btreep.h"

//create bleaf struct and file, and return struct
BLEAF* bleaf_create(BTREEP* btreep, unsigned long parent_file, unsigned int num_keys, unsigned long previous_leaf, unsigned long next_leaf){

	if( !btreep ) return NULL;

	BLEAF* bleaf = (BLEAF*)bnode_create(btreep, 1, num_keys, parent_file);

	bleaf->previous_leaf = previous_leaf;
	bleaf->next_leaf = next_leaf;

	bleaf_save(btreep, bleaf);

	return bleaf;
}

//save bleaf struct to file
void bleaf_save(BTREEP* btreep, BLEAF* bleaf){

	if( !btreep || !bleaf ) return;

	//save bnode part
	bnode_save(btreep, (BNODE*)bleaf);

	//save previous leaf
	fwrite(&bleaf->previous_leaf, sizeof(unsigned long), 1, bleaf->file);

	//save next leaf
	fwrite(&bleaf->next_leaf, sizeof(unsigned long), 1, bleaf->file);

	//actual data stored in the leaf is always handled in secondary memory so its already saved
}

//load bleaf struct from file
BLEAF* bleaf_load(BTREEP* btreep, unsigned long id){

	//read bnode part
	BLEAF* bleaf = (BLEAF*)bnode_load(btreep, id);

	//return NULL on error
	if( !bleaf ) return NULL;

	//read previous leaf
	fread(&bleaf->previous_leaf, sizeof(unsigned long), 1, bleaf->file);

	//read next_leaf
	fread(&bleaf->next_leaf, sizeof(unsigned long), 1, bleaf->file);

	return bleaf;
}

//free struct, saving it to file first
void bleaf_free(BTREEP* btreep, BLEAF* bleaf){

	if( !btreep || !bleaf ) return;

	//save it
	bleaf_save(btreep, bleaf);

	//close file
	fclose(bleaf->file);

	//just free struct, we got no pointers in it except the file we closed
	free(bleaf);
}

/*
	POLYMORPHISM FUNCTIONS
*/

//get key index
unsigned int bleaf_get_key_index(BTREEP* btreep, BLEAF* bleaf, void* key){

	if( !btreep || !bleaf || !key ) return -1;

	void* data;

	//iterate through data
	for(unsigned int i=0; i < bleaf->num_keys; i++){

		//get data at this index
		data = bleaf_get_data_at(btreep, bleaf, i);

		//see if the key at data match the one given
		if( !memcmp(btreep->key(data), key, btreep->key_len) ){

			btreep->free_data(data);
			return i;
		}
		btreep->free_data(data);
	}

	return -1;
}

//return data from index
void* bleaf_get_data_at(BTREEP* btreep, BLEAF* bleaf, unsigned int index){

	if( !btreep || !bleaf || index >= bleaf->num_keys ) return NULL;

	//allocate memory for data
	void* data = malloc(btreep->data_len);

	//offset = bnode + previous_leaf + next_leaf = 29 bytes
	//offset + (btreep->data_len * index) = data position

	//move cursor to data position, calculated using the
	//logic above
	fseek(bleaf->file, 29 + (btreep->data_len * index), SEEK_SET);

	//read data
	fread(data, btreep->data_len, 1, bleaf->file);

	return data;
}

//return key in data at index
void* bleaf_get_key_at(BTREEP* btreep, BLEAF* bleaf, unsigned int index){

	void* data = bleaf_get_data_at(btreep, bleaf, index);

	if( !data ) return NULL;

	void* key = malloc(btreep->key_len);
	memcpy(key, btreep->key(data), btreep->key_len);

	btreep->free_data(data);
	return key;
}

//set data at the index given
unsigned char bleaf_set_data_at(BTREEP* btreep, BLEAF* bleaf, unsigned int index, void* data){

	if( !btreep || !bleaf || index >= bleaf->num_keys ) return 0;

	//go to data position, using the logic shown in bleaf_get_data_at function
	fseek(bleaf->file, 29 + (btreep->data_len * index), SEEK_SET);

	//write data
	fwrite(data, btreep->data_len, 1, bleaf->file);

	return 1;
}

//remove data at index and return it
void* bleaf_remove_data_at(BTREEP* btreep, BLEAF* bleaf, unsigned int index){

	if( !btreep || !bleaf || index >= bleaf->num_keys ) return NULL;

	//get data to return later
	void* to_return = bleaf_get_data_at(btreep, bleaf, index);

	void* data;

	//iterate through datum and copy it to previous position
	for(unsigned int i = index+1; i < bleaf->num_keys; i++){

		data = bleaf_get_data_at(btreep, bleaf, i);
		bleaf_set_data_at(btreep, bleaf, i-1, data);
		btreep->free_data(data);
	}

	//truncate file, 'deleting' the last element
	ftruncate(fileno(bleaf->file), 29 + btreep->data_len * (--bleaf->num_keys));

	//return removed data
	return to_return;
}

//insert data at right position in the leaf, returns index
unsigned int bleaf_insert_data(BTREEP* btreep, BLEAF* bleaf, void* data){

	if( !btreep || !bleaf || !data ) return -1;

	void* file_data;

	long i;
	unsigned int index = -1;

	bleaf->num_keys++;

	for(i = bleaf->num_keys-1; i > 0 && index == -1; i--){

		file_data = bleaf_get_data_at(btreep, bleaf, i-1);

		if( btreep->cmp(btreep->key(file_data), btreep->key(data)) ){

			bleaf_set_data_at(btreep, bleaf, i, data);
			index = i;
		}else{

			bleaf_set_data_at(btreep, bleaf, i, file_data);
		}

		btreep->free_data(file_data);
	}

	if( index == -1 ){

		index = 0;
		bleaf_set_data_at(btreep, bleaf, index, data);
	}

	return index;
}

//split leaf, returning the new leaf
unsigned long bleaf_split(BTREEP* btreep, unsigned long id){

	if( !btreep || !id ) return 0;

	//load given node
	BLEAF* bleaf = bleaf_load(btreep, id);

	//create new leaf
	BLEAF* new_bleaf = bleaf_create(btreep,
					bleaf->parent_file,
					0,
					bleaf->previous_leaf,
					bleaf->id);

	//update previous/next leaf
	bleaf->previous_leaf = new_bleaf->id;

	if( new_bleaf->previous_leaf ) bleaf_set_next_leaf(new_bleaf->previous_leaf, new_bleaf->id);

	void* data;

	//move data from old leaf to new one
	for(unsigned int i = 0; i < btreep->t - 1; i++){

		//pop data
		data = bleaf_remove_data_at(btreep, bleaf, 0);

		//insert in the new node
		bleaf_insert_data(btreep, new_bleaf, data);

		//free it
		btreep->free_data(data);
	}

	void* mid_key = bleaf_get_key_at(btreep, bleaf, 0);

	//save ids for later and free structs
	unsigned long bleaf_id = bleaf->id;
	unsigned long new_bleaf_id = new_bleaf->id;
	unsigned long parent_id = bleaf->parent_file;

	bleaf_free(btreep, bleaf);
	bleaf_free(btreep, new_bleaf);

	BINDEX* parent;

	//create parent node if leaf is root
	if( !parent_id ){

		unsigned long parent_intervals[] = {new_bleaf_id, bleaf_id};

		parent = bindex_create(btreep,
					0,
					1,
					&mid_key,
					parent_intervals);

		//update parent file field in children
		bnode_set_parent_file(bleaf_id, parent->id);

		//make it the new root
		btreep_struct_change_root(btreep, parent->id);

	}else{

		//split parent if necessary
		if( !bnode_can_insert(parent_id, btreep->t) )
			bindex_split(btreep, parent_id);

		parent = bindex_load(btreep, bnode_get_parent_file(bleaf_id));

		unsigned int index =  bindex_insert_key(btreep, parent, mid_key);

		bindex_insert_interval_at(btreep, parent, index, new_bleaf_id);	
	}
	
	btreep->free_key(mid_key);

	bnode_set_parent_file(new_bleaf_id, parent->id);

	//close and save structs
	bindex_free(btreep, parent);

	return new_bleaf_id;
}

//steal from sibling, return 1 if we can steal and everything went ok
unsigned char bleaf_steal_from_sibling(BTREEP* btreep, unsigned long id){

	if( !btreep || !id ) return 0;

	//get the receiver node
	BLEAF* receiver = bleaf_load(btreep, id);

	//sibling
	BLEAF* sender;

	unsigned char steal_from_right;

	if( bnode_can_remove(receiver->next_leaf, btreep->t) ){

		sender = bleaf_load(btreep, receiver->next_leaf);
		steal_from_right = 1;

	}else if( bnode_can_remove(receiver->previous_leaf, btreep->t) ){

		sender = bleaf_load(btreep, receiver->previous_leaf);
		steal_from_right = 0;
	}else{

		bleaf_free(btreep, receiver);

		//look like we can't remove from none of the sibling :(
		return 0;
	}

	//let's steal!

	//get parent node
	BINDEX* parent = bindex_load(btreep, receiver->parent_file);

	//first we need the index of the key between the two leaves (the parent key)
	unsigned int parent_key_index = bindex_get_interval_index(btreep, parent, sender->id ) - steal_from_right;

	//remove key from sender
	void* data = bleaf_remove_data_at(btreep, sender, steal_from_right ? 0 : sender->num_keys-1);

	//insert in receiver
	bleaf_insert_data(btreep, receiver, data);

	//update parent key
	if( steal_from_right ){

		void* key = bleaf_get_key_at(btreep, sender, 0);

		bindex_set_key_at(btreep, parent, parent_key_index, key);

		btreep->free_key(key);
	}else{

		bindex_set_key_at(btreep, parent, parent_key_index, btreep->key(data));
	}

	//free data
	btreep->free_data(data);

	//free structs
	bindex_free(btreep, parent);
	bleaf_free(btreep, receiver);
	bleaf_free(btreep, sender);

	//return ok
	return 1;
}

//merge with sibling, return 1 if we can merge and everything went ok
unsigned char bleaf_merge_with_sibling(BTREEP* btreep, unsigned long id){

	if( !btreep || !id ) return 0;

	//get receiver node
	BLEAF* receiver = bleaf_load(btreep, id);

	//sibling
	BLEAF* sender;

	unsigned char steal_from_right;

	unsigned long left_sibling = bnode_get_left_sibling(btreep, (BNODE*)receiver);
	unsigned long right_sibling = bnode_get_right_sibling(btreep, (BNODE*)receiver);

	if( right_sibling ){

		sender = bleaf_load(btreep, right_sibling);
		steal_from_right = 1;

	}else if( left_sibling ){

		sender = bleaf_load(btreep, left_sibling);
		steal_from_right = 0;
	}else{

		bleaf_free(btreep, receiver);

		//in this case we have no siblings (shouldn't happen so we just return 0)
		return 0;
	}

	//let's merge

	void* data;

	//send all keys from sender to receiver
	for(unsigned int i=0; sender->num_keys; i++){

		data = bleaf_remove_data_at(btreep, sender, 0);
		bleaf_insert_data(btreep, receiver, data);
		btreep->free_data(data);
	}

	//if the parent is root and have only one key, just make the receiver the new root
	if( sender->parent_file == *(unsigned long*)btreep->root->data && bnode_get_num_keys(sender->parent_file) == 1 ){

		//change root
		btreep_struct_change_root(btreep, receiver->id);

		//remove old root node file and id from list
		btreep_struct_remove_id(btreep, receiver->parent_file);

		//change receiver's parent file field
		receiver->parent_file = 0;
	}else{

		//only leave receiver's interval and delete the key between receiver and sender
		BINDEX* parent = bindex_load(btreep, receiver->parent_file);

		//get key index
		unsigned int parent_key_index = bindex_get_interval_index(btreep, parent, sender->id) - steal_from_right;

		//delete key
		btreep->free_key(bindex_remove_key_at(btreep, parent, parent_key_index));

		//delete sender's interval
		bindex_remove_interval_at(btreep, parent, parent_key_index + steal_from_right);
	
		//free parent struct
		bindex_free(btreep, parent);
	}

	//save sender id
	unsigned long sender_id = sender->id;

	//update next leaf and previous leaf fields
	if( steal_from_right ){

		receiver->next_leaf = sender->next_leaf;
		if( sender->next_leaf ) bleaf_set_previous_leaf(sender->next_leaf, receiver->id);
	}else{

		receiver->previous_leaf = sender->previous_leaf;
		if( sender->previous_leaf ) bleaf_set_next_leaf(sender->previous_leaf, receiver->id);
	}

	//free structs
	bleaf_free(btreep, receiver);
	bleaf_free(btreep, sender);

	//remove sender
	char filename[30];
	sprintf(filename, "btreep_data/%u.dat", sender_id);
	remove(filename);

	//return ok
	return 1;
}

/*
	QUICK READINGS
*/

unsigned long bleaf_get_previous_leaf(unsigned long id){

	if( !id ) return 0;

	//open file
	FILE* file;

	char filename[30];
	sprintf(filename, "btreep_data/%u.dat", id);

	if( !( file = fopen(filename, "r+b")) ) return 0;

	unsigned long previous_leaf;

	//got to position
	fseek(file, 13, SEEK_SET);

	//get previous_leaf
	fread(&previous_leaf, sizeof(unsigned long), 1, file);

	fclose(file);

	return previous_leaf;
}

unsigned long bleaf_get_next_leaf(unsigned long id){

	if( !id ) return 0;

	//open file
	FILE* file;

	char filename[30];
	sprintf(filename, "btreep_data/%u.dat", id);

	if( !( file = fopen(filename, "r+b")) ) return 0;

	unsigned long next_leaf;

	//got to position
	fseek(file, 21, SEEK_SET);

	//get previous_leaf
	fread(&next_leaf, sizeof(unsigned long), 1, file);

	fclose(file);

	return next_leaf;
}


/*
	QUICK WRITINGS
*/

void bleaf_set_previous_leaf(unsigned long id, unsigned long new_previous_leaf){

	if( !id ) return;

	//open file
	FILE* file;

	char filename[30];
	sprintf(filename, "btreep_data/%u.dat", id);

	if( !( file = fopen(filename, "r+b")) ) return;

	//got to position
	fseek(file, 13, SEEK_SET);

	//get previous_leaf
	fwrite(&new_previous_leaf, sizeof(unsigned long), 1, file);

	fclose(file);
}

void bleaf_set_next_leaf(unsigned long id, unsigned long new_next_leaf){

	if( !id ) return;

	//open file
	FILE* file;

	char filename[30];
	sprintf(filename, "btreep_data/%u.dat", id);

	if( !( file = fopen(filename, "r+b")) ) return;

	//got to position
	fseek(file, 21, SEEK_SET);

	//get previous_leaf
	fwrite(&new_next_leaf, sizeof(unsigned long), 1, file);

	fclose(file);
}


