#ifndef BTREEP_LEAF_H
#define BTREEP_LEAF_H

//NOTE: the data being kept at leaves shall never go in the primary memory
//since it can be like REALLY BIG (that's the idea at least)
//"oh, but why don't we do this with the keys then?". i'm glad you asked. the key normally shouldn't be really big
//if they are, blame the user. say something like:
//"using big keys is often unecessary or unefficient so we disincourage this practice by erasing the possibility"
//see? sounds fancy and all, almost like you actually put some thought on it instead of being lazy

/*

	BLEAF FILE STRUCTURE:

	  13 bytes       8 bytes       8 bytes
	|---------------------------------------|
	|   bnode   | previous_leaf | next_leaf | 
	|---------------------------------------|

*/

typedef struct BLEAF{

	//id of this node
	unsigned long id;

	//1 if leaf, 0 otherwise
	unsigned char is_leaf;

	//number of keys in this node
	unsigned int num_keys;

	//id of the parent
	unsigned long parent_file;

	//file pointer to this node in secondary memory
	FILE* file;

	//id of the previous leaf
	unsigned long previous_leaf;

	//id of the next leaf
	unsigned long next_leaf;

}BLEAF;

//create bleaf struct and file, and return struct
BLEAF* bleaf_create(
			BTREEP*,
			unsigned long parent_file,
			unsigned int num_keys,
			unsigned long previous_leaf,
			unsigned long next_leaf
			);

//save bleaf struct to file
void bleaf_save(BTREEP*, BLEAF*);

//load bleaf struct from file
BLEAF* bleaf_load(BTREEP*, unsigned long id);

//free struct, saving it to file first
void bleaf_free(BTREEP*, BLEAF*);

/*
	POLYMORPHISM FUNCTIONS
*/

//get key index
unsigned int bleaf_get_key_index(BTREEP*, BLEAF*, void* key);

//return data from index
void* bleaf_get_data_at(BTREEP*, BLEAF*, unsigned int index);

//return key in data at index
void* bleaf_get_key_at(BTREEP*, BLEAF*, unsigned int index);

//set data at the index given
unsigned char bleaf_set_data_at(BTREEP*, BLEAF*, unsigned int index, void* data);

//remove data at index and return it
void* bleaf_remove_data_at(BTREEP*, BLEAF*, unsigned int index);

//insert data at right position in the leaf, returns index
unsigned int bleaf_insert_data(BTREEP*, BLEAF*, void* data);

//split leaf, returning the new leaf
unsigned long bleaf_split(BTREEP*, unsigned long id);

//steal from sibling, return 1 if we can steal and everything went ok
unsigned char bleaf_steal_from_sibling(BTREEP*, unsigned long id);

//merge with sibling, return 1 if we can merge and everything went ok
unsigned char bleaf_merge_with_sibling(BTREEP*, unsigned long id);

/*
	QUICK READINGS
*/

unsigned long bleaf_get_previous_leaf(unsigned long id);

unsigned long bleaf_get_next_leaf(unsigned long id);


/*
	QUICK WRITINGS
*/

void bleaf_set_previous_leaf(unsigned long id, unsigned long new_previous_leaf);

void bleaf_set_next_leaf(unsigned long id, unsigned long new_next_leaf);

#endif
