#ifndef BTREEP_H
#define BTREEP_H

#define debug fprintf(stderr, "%s:%d\n", __FILE__, __LINE__);

//c includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> //ftruncate

//lib includes

//linked list lib using void
#include "linked_list/linked_list.h"

//btreep struct (keep track of nodes ids)
#include "btreep_struct/btreep_struct.h"

//abstract node struct and functions
#include "bnode/bnode.h"

//index node
#include "bindex/bindex.h"

//leaf node
#include "bleaf/bleaf.h"

//print
#include "bprint/bprint.h"

//search
#include "bsearch/bsearch.h"

//insert
#include "binsert/binsert.h"

//remove
#include "bremove/bremove.h"

#endif
