#include "../btreep.h"

//main search function, return data for given key (return NULL if none is found)
void* btreep_search(BTREEP* btreep, void* key){

	if( !btreep || !btreep->root || !key ) return NULL;

	//get leaf where it should be at
	unsigned long final_leaf = btreep_search_final_leaf(btreep, key);

	//get node struct
	BLEAF* bleaf = bleaf_load(btreep, final_leaf);

	//return data at key index (will return NULL if not found)
	void* data = bleaf_get_data_at(btreep, bleaf, bleaf_get_key_index(btreep, bleaf, key));

	fclose(bleaf->file);
	free(bleaf);
	return data;
}

//return next subtree root
unsigned long btreep_search_next_subtree_root(BTREEP* btreep, unsigned long id, void* key){

	if( !btreep || !id || !key ) return 0;

	//get bindex struct
	BINDEX* bindex = bindex_load(btreep, id);

	unsigned long next_subtree_root = 0;

	//iterate through keys
	for(unsigned int i = 0; i < bindex->num_keys && !next_subtree_root; i++){

		//check if key fits here
		if( btreep->cmp(key, bindex->keys[i]) )
			next_subtree_root = bindex->intervals[i];
	}

	//return last interval if given key is greater than all the others
	next_subtree_root = next_subtree_root ?: bindex->intervals[bindex->num_keys];

	fclose(bindex->file);
	free(bindex->intervals);
	for(unsigned int i=0; i < bindex->num_keys; i++) btreep->free_key(bindex->keys[i]);
	free(bindex->keys);
	free(bindex);

	return next_subtree_root;
}

//return leaf where the key SHOULD be (it can happen that the key just doesn't exist in the tree)
unsigned long btreep_search_final_leaf(BTREEP* btreep, void* key){

	if( !btreep || !key ) return 0;

	unsigned long id;

	do{

		id = btreep_search_next_subtree_root(btreep, id, key);

	}while(!bnode_get_is_leaf(id));

	return id;
}
