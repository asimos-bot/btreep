#ifndef BTREEP_SEARCH_H
#define BTREEP_SEARCH_H

//main search function, return data for given key (return NULL if none is found)
void* btreep_search(BTREEP*, void* key);

//return next subtree root
unsigned long btreep_search_next_subtree_root(BTREEP*, unsigned long id, void* key);

//return leaf where the key SHOULD be (it can happen that the key just doesn't exist in the tree)
unsigned long btreep_search_final_leaf(BTREEP*, void* key);

#endif
