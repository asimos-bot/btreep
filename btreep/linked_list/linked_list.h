#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include<stdlib.h>
#include<stdio.h>
#include<string.h> //memcpy

typedef struct LIST{

	void* data;
	unsigned int data_len;

	struct LIST* next;
}LIST;

//allocate memory for list
LIST* list_create();

//free data from node
void list_free_node(LIST*);

//free list, including all nodes after it
void list_free(LIST*);

//allocate memory for data
void list_alloc(LIST*, unsigned int);

//get node where node->next==NULL
LIST* list_get_front(LIST*);

//print only this node
void list_print_node(LIST*);

//print nodes data
void list_print(LIST*);

//insert node immediatly after first argument node (return first argument or second if first was null)
LIST* list_insert_node(LIST*, LIST*);

//create node with data and insert it after the given node (return first argument or new list if it was null)
LIST* list_insert(LIST*, void*, unsigned int);

//remove node from list and return it
LIST* list_remove_node(LIST*, LIST*);

//remove node with given data and data_len and return it
LIST* list_remove(LIST*, void*, unsigned int);

//search for node with data, or NULL if it can't be found
LIST* list_search(LIST*, void*, unsigned int);

#endif
