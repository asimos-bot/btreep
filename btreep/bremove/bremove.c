#include "../btreep.h"

//remove key if it exists
void btreep_remove(BTREEP* btreep, void* key){

	if( !btreep || !key ) return;

	unsigned long x, y;

	x = *(unsigned long*)btreep->root->data;

	while( !bnode_get_is_leaf(x) ){

		y = btreep_search_next_subtree_root(btreep, x, key);

		if( !bnode_can_remove(y, btreep->t) && !btreep_steal_from_sibling(btreep, y))

			//if we can't steal from sibling, merge with it (se n pode vence-los, junte-se a eles)
			btreep_merge_with_sibling(btreep, y);

		x = y;
	}

	//just remove key from leaf
	BLEAF* bleaf = bleaf_load(btreep, x);

	btreep->free_data(bleaf_remove_data_at(btreep, bleaf, bleaf_get_key_index(btreep, bleaf, key)));

	bleaf_free(btreep, bleaf);
}

//steal a key from a sibling, if it isn't possible return 0
unsigned char btreep_steal_from_sibling(BTREEP* btreep, unsigned long x){
printf("%lu stealing from sibling\n", x);
	return bnode_get_is_leaf(x) ? bleaf_steal_from_sibling(btreep, x) : bindex_steal_from_sibling(btreep, x);
}

//merge with a sibling with t-1 keys
unsigned char btreep_merge_with_sibling(BTREEP* btreep, unsigned long x){
printf("%lu merging with sibling\n",x );
	return bnode_get_is_leaf(x) ? bleaf_merge_with_sibling(btreep, x) : bindex_merge_with_sibling(btreep, x);
}
