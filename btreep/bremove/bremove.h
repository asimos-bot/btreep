#ifndef BTREEP_REMOVE_H
#define BTREEP_REMOVE_H

//remove key if it exists
void btreep_remove(BTREEP*, void* key);

//steal a key from a sibling, if it isn't possible return 0
unsigned char btreep_steal_from_sibling(BTREEP* btreep, unsigned long x);

//merge with a sibling with t-1 keys
unsigned char btreep_merge_with_sibling(BTREEP* btreep, unsigned long x);

#endif
