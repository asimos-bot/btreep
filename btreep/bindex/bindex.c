#include "../btreep.h"

//create file and return bindex struct
BINDEX* bindex_create(BTREEP* btreep, unsigned long parent_file, unsigned int num_keys, void** keys, unsigned long* intervals){

	if( !btreep || !num_keys || !keys || !intervals ) return NULL;

	//create generic node
	BINDEX* bindex = (BINDEX*)bnode_create(btreep, 0, num_keys, parent_file);

	//allocate memory space for the keys pointers
	bindex->keys = malloc(sizeof(void*) * num_keys);

	//allocate memory for intervals
	bindex->intervals = malloc(sizeof(unsigned long) * (num_keys+1));

	//copy intervals
	memcpy(bindex->intervals, intervals, (num_keys+1) * sizeof(unsigned long));

	//iterate over keys and intervals
	for(unsigned int i=0; i < num_keys; i++){

		//allocate memory space for this key
		bindex->keys[i] = malloc(sizeof(btreep->key_len));
	
		//copy key
		memcpy(bindex->keys[i], keys[i], btreep->key_len);
	}

	//write to file
	bindex_save(btreep, bindex);

	return bindex;
}

//save bindex struct to file
void bindex_save(BTREEP* btreep, BINDEX* bindex){

	if( !btreep || !bindex ) return;	

	//save bnode part
	bnode_save(btreep, (BNODE*)bindex);

	//write first interval to file
	fwrite(&bindex->intervals[0], sizeof(unsigned long), 1, bindex->file);

	//iterate over keys and intervals
	for(unsigned int i=0; i < bindex->num_keys; i++){

		//write key to file
		fwrite(bindex->keys[i], btreep->key_len, 1, bindex->file);
	
		//write interval to file
		fwrite(&bindex->intervals[i+1], sizeof(unsigned long), 1, bindex->file);
	}
}

//load bindex struct from file
BINDEX* bindex_load(BTREEP* btreep, unsigned long id){

	//load bnode part
	BINDEX* bindex = (BINDEX*)bnode_load(btreep, id);

	//return NULL on error
	if( !bindex ) return NULL;

	//allocate memory for intervals
	bindex->intervals = malloc(sizeof(unsigned long) * (bindex->num_keys+1));

	//allocate memory for key pointer vector
	bindex->keys = malloc(sizeof(void*) * bindex->num_keys);

	//read first interval
	fread(&bindex->intervals[0], sizeof(unsigned long), 1, bindex->file);

	//iterate through keys and intervals to read them
	for(unsigned int i=0; i < bindex->num_keys; i++){

		//allocate memory for this key
		bindex->keys[i] = malloc(btreep->key_len);

		//read key
		fread(bindex->keys[i], sizeof(btreep->key_len), 1, bindex->file);

		//read interval
		fread(&bindex->intervals[i+1], sizeof(unsigned long), 1, bindex->file);
	}

	return bindex;
}

//free bindex struct, saving it before
void bindex_free(BTREEP* btreep, BINDEX* bindex){

	if( !btreep || !bindex ) return;

	//save it first
	bindex_save(btreep, bindex);

	//free intervals
	free(bindex->intervals);

	//free each key
	for(unsigned int i=0; i < bindex->num_keys; i++) btreep->free_key(bindex->keys[i]);

	//free key pointer vector
	free(bindex->keys);

	//close file
	fclose(bindex->file);

	//free struct itself
	free(bindex);
}

/*
	POLYMORPHISM FUNCTIONS
*/

//return index of the element with the key given (return -1 on error)
unsigned int bindex_get_key_index(BTREEP* btreep, BINDEX* bindex, void* key){

	if( !btreep || !bindex || !key ) return -1;

	//iterate through keys and return the one matching
	for(unsigned int i=0; i < bindex->num_keys; i++)

		if( !memcmp(bindex->keys[i], key, btreep->key_len) )
			return i;

	//if none match, return -1
	return -1;
}

//return key/data at given index (return NULL on error, like out of bounds and others)
//NOTE: return the actual pointer, not a copy
void* bindex_get_key_at(BTREEP* btreep, BINDEX* bindex, unsigned int index){

	if( !btreep || !bindex || index >= bindex->num_keys ) return NULL;

	//return desired index pointer
	return bindex->keys[index];
}

//set value of key at given index (return 1 if operation was sucessful)
unsigned char bindex_set_key_at(BTREEP* btreep, BINDEX* bindex, unsigned int index, void* key){

	if( !btreep || !bindex || index >= bindex->num_keys || !key ) return 0;

	//copy key
	memcpy(bindex->keys[index], key, btreep->key_len);

	//return 1, since the operation was sucessful
	return 1;
}

//remove key at given index and returns it (return NULL on error)
void* bindex_remove_key_at(BTREEP* btreep, BINDEX* bindex, unsigned int index){

	if( !btreep || !bindex || index >= bindex->num_keys ) return NULL;

	//save pointer to return later
	void* to_return = bindex->keys[index];

	//move all pointers in front of the given index
	//to the previous pointer
	for(unsigned int i=index+1; i < bindex->num_keys; i++)
		bindex->keys[i-1] = bindex->keys[i];

	//realloc vector of pointers, to 'delete' last element
	bindex->keys = realloc(bindex->keys, sizeof(void*)*(--bindex->num_keys));

	//return the pointer we saved before (the pointer removed)
	return to_return;
}

//insert key in the right position, returns its index
unsigned int bindex_insert_key(BTREEP* btreep, BINDEX* bindex, void* key){

	if( !btreep || !bindex || !key ) return -1;

	unsigned int i;

	//iterate through keys, until we find one which is
	//less to the one given
	for(i=0; i < bindex->num_keys && btreep->cmp(bindex->keys[i], key); i++);

	//save i value
	unsigned index = i;

	//realloc vector of pointers to have space for new element
	bindex->keys = realloc(bindex->keys, sizeof(void*)*(++bindex->num_keys));

	//move all elements from the i-th position and
	//in front of it to the next position
	for(i = bindex->num_keys-1; i > index; i--)
		bindex->keys[i] = bindex->keys[i-1];

	//allocate memory for new key
	bindex->keys[index] = malloc(btreep->key_len);

	//copy key
	memcpy(bindex->keys[index], key, btreep->key_len);

	//return index of new key
	return index;
}

/*
	BINDEX SPECIFIC FUNCTIONS
*/

//get index of interval
unsigned int bindex_get_interval_index(BTREEP* btreep, BINDEX* bindex, unsigned long interval){

	if( !btreep || !bindex || !interval ) return -1;

	//iterate through intervals
	for(unsigned int i=0; i <= bindex->num_keys; i++)

		//return matching interval index
		if( bindex->intervals[i] == interval ) return i;

	//return -1 (none was found)
	return -1;
}

//return interval at index
unsigned long bindex_get_interval_at(BTREEP* btreep, BINDEX* bindex, unsigned int index){

	if( !btreep || !bindex || index > bindex->num_keys ) return 0;

	return bindex->intervals[index];
}

//set interval at index (return 1 on sucess)
unsigned char bindex_set_interval_at(BTREEP* btreep, BINDEX* bindex, unsigned int index, unsigned long interval){

	if( !btreep || !bindex || index > bindex->num_keys || !interval ) return 0;

	bindex->intervals[index] = interval;

	return 1;
}

//insert new interval at index, return 1 on sucess, 0 otherwise
//NOTE: doesnt change bindex->num_keys
unsigned char bindex_insert_interval_at(BTREEP* btreep, BINDEX* bindex, unsigned int index, unsigned long interval){

	if( !btreep || !bindex || index > bindex->num_keys || !interval ) return 0;

	//realloc vector of intervals to fit a new element in it
	bindex->intervals = realloc(bindex->intervals, sizeof(unsigned long) * (bindex->num_keys+1));

	//move all intervals in front of
	//the given index to the next position
	for(unsigned int i = bindex->num_keys; i > index; i--)
		bindex->intervals[i] = bindex->intervals[i-1];

	//insert interval at the given index
	bindex->intervals[index] = interval;

	return 1;
}

//remove interval at index and return it
//NOTE: doesnt change bindex->num_keys
unsigned long bindex_remove_interval_at(BTREEP* btreep, BINDEX* bindex, unsigned int index){

	if( !btreep || !bindex || index > bindex->num_keys + 1 ) return 0;

	//save interval
	unsigned long to_return = bindex->intervals[index];

	//we will change bindex->num_keys back later
	bindex->num_keys++;

	//move all elements in front of index to the previous position
	for(unsigned int i=index+1; i <= bindex->num_keys; i++)
		bindex->intervals[i-1] = bindex->intervals[i];

	//realloc vector to 'delete' last element
	bindex->intervals = realloc(bindex->intervals, sizeof(unsigned long) * (bindex->num_keys));

	//get bindex->num_keys to its previous value
	bindex->num_keys--;

	return to_return;
}

//split y, returning new node
unsigned long bindex_split(BTREEP* btreep, unsigned long y){

	if( !btreep ) return 0;

	//get bindex struct
	BINDEX* bindex = bindex_load(btreep, y);

	//lets save the keys and intervals we will use for the new node
	void* keys[btreep->t-1];
	unsigned long intervals[btreep->t];

	for(unsigned int i = 0; i < btreep->t - 1; i++){

		keys[i] = bindex_remove_key_at(btreep, bindex, 0);

		intervals[i] = bindex_remove_interval_at(btreep, bindex, 0);
	}

	//get mid key
	void* mid_key = bindex_remove_key_at(btreep, bindex, 0);

	//add last interval
	intervals[btreep->t-1] = bindex_remove_interval_at(btreep, bindex, 0);

	//create new node
	BINDEX* new_bindex = bindex_create(btreep,
					bindex->parent_file,
					btreep->t - 1,
					keys,
					intervals);

	//get ids used later and free structs 
	unsigned long bindex_id = bindex->id;
	unsigned long new_bindex_id = new_bindex->id;
	unsigned long parent_id = bindex->parent_file;

	bindex_free(btreep, new_bindex);
	bindex_free(btreep, bindex);

	BINDEX* parent;

	if( !parent_id ){

		unsigned long parent_intervals[2] = {new_bindex_id, bindex_id};

		//create a parent if the node didn't have one before
		parent = bindex_create(btreep, 0, 1, &mid_key, parent_intervals);

		//change root
		btreep_struct_change_root(btreep, parent->id);

		//update parent field in children
		bnode_set_parent_file(bindex_id, parent->id);
	}else{

		//insert mid key in parent, splitting before if necessary
		if( !bnode_can_insert(parent_id, btreep->t) )
			bindex_split(btreep, parent_id);
	
		parent = bindex_load(btreep, bnode_get_parent_file(bindex_id));

		unsigned int index = bindex_insert_key(btreep, parent, mid_key);
		bindex_insert_interval_at(btreep, parent, index, new_bindex_id);	
	}

	btreep->free_key(mid_key);
	
	bnode_set_parent_file(new_bindex_id, parent->id);

	//close and save structs
	bindex_free(btreep, parent);

	btreep_update_childs(btreep, new_bindex_id);

	//free keys (copies were made when creating the new node)
	for(unsigned int i=0; i < btreep->t - 1; i ++) btreep->free_key(keys[i]);

	return new_bindex_id;
}

//steal key from sibling, return 1 if ok
unsigned char bindex_steal_from_sibling(BTREEP* btreep, unsigned long id){

	if( !btreep || !id ) return 0;

	//receiver node
	BINDEX* receiver = bindex_load(btreep, id);

	BINDEX* sender;
	unsigned char steal_from_right;

	unsigned long right_sibling = bnode_get_right_sibling(btreep, (BNODE*)receiver);
	unsigned long left_sibling = bnode_get_left_sibling(btreep, (BNODE*)receiver);

	if( bnode_can_remove(right_sibling, btreep->t) ){

		sender = bindex_load(btreep, right_sibling);
		steal_from_right = 1;

	}else if( bnode_can_remove(left_sibling, btreep->t) ){

		sender = bindex_load(btreep, left_sibling);
		steal_from_right = 0;
	}else{

		bindex_free(btreep, receiver);
		return 0;
	}

	BINDEX* parent = bindex_load(btreep, receiver->parent_file);

	//get parent key index
	unsigned int parent_key_index = bindex_get_interval_index(btreep, parent, sender->id) - steal_from_right;

	//insert parent key in receiver
	bindex_insert_key(btreep, receiver, parent->keys[parent_key_index]);

	//free old parent key
	btreep->free_key(parent->keys[parent_key_index]);

	//set new parent key
	parent->keys[parent_key_index] = bindex_remove_key_at(btreep, sender, steal_from_right ? 0 : sender->num_keys - 1 );

	unsigned long interval = bindex_remove_interval_at(btreep, sender, steal_from_right ? 0 : sender->num_keys + 1 );

	//fix intervals by removing a interval from sender and inserting in receiver
	bindex_insert_interval_at(btreep, receiver, steal_from_right ? receiver->num_keys : 0, interval);

	//set parent_file field in the interval moved
	bnode_set_parent_file(interval, receiver->id);

	//free structs
	bindex_free(btreep, parent);
	bindex_free(btreep, receiver);
	bindex_free(btreep, sender);

	//return ok
	return 1;
}

//merge with a sibling, return 1 if ok
unsigned char bindex_merge_with_sibling(BTREEP* btreep, unsigned long id){

	if( !btreep || !id ) return 0;

	//receiver node
	BINDEX* receiver = bindex_load(btreep, id);

	BINDEX* sender;
	unsigned char steal_from_right;

	unsigned long right_sibling = bnode_get_right_sibling(btreep, (BNODE*)receiver);
	unsigned long left_sibling = bnode_get_left_sibling(btreep, (BNODE*)receiver);

	if( right_sibling ){

		sender = bindex_load(btreep, right_sibling);
		steal_from_right=1;
	}else{

		sender = bindex_load(btreep, left_sibling);
		steal_from_right=0;
	}

	BINDEX* parent = bindex_load(btreep, receiver->parent_file);

	//get parent key index
	unsigned int parent_key_index = bindex_get_interval_index(btreep, parent, sender->id) - steal_from_right;

	//get mid key from parent
	void* key = bindex_remove_key_at(btreep, parent, parent_key_index);

	//insert key 
	bindex_insert_key(btreep, receiver, key);

	//free key
	btreep->free_key(key);

	//insert first interval from sender in receiver
	bindex_insert_interval_at(btreep, receiver, steal_from_right ? receiver->num_keys : 0, sender->intervals[steal_from_right ? 0 : btreep->t-1 ]);

	//move all keys and intervals from sender to receiver
	for(unsigned int i = 1; i < btreep->t; i++){

		//insert key
		bindex_insert_key(btreep, receiver, sender->keys[steal_from_right ? i-1 : btreep->t-i]);

		//insert interval
		bindex_insert_interval_at(btreep, receiver, steal_from_right ? receiver->num_keys : 0, sender->intervals[steal_from_right ? i : btreep->t-1-i ]);
	}

	//if parent node is root and have no key left
	if( parent->id == *(unsigned long*)btreep->root->data && !parent->num_keys ){

		//save parent id
		unsigned long parent_id = parent->id;

		//free parent struct
		bindex_free(btreep, parent);

		//change root
		btreep_struct_change_root(btreep, receiver->id);

		//remove old root from list
		btreep_struct_remove_id(btreep, parent_id);

		//update receiver parent_file
		receiver->parent_file = 0;
	}else{

		//remove sender's id from interval list in parent
		bindex_remove_interval_at(btreep, parent, parent_key_index + steal_from_right);

		bindex_free(btreep, parent);
	}

	//save ids
	unsigned long receiver_id = receiver->id;
	unsigned long sender_id = sender->id;

	//free structs
	bindex_free(btreep, receiver);
	bindex_free(btreep, sender);

	//remove sender
	btreep_struct_remove_id(btreep, sender_id);

	//update childs
	btreep_update_childs(btreep, receiver_id);

	//return ok
	return 1;
}
