#ifndef BTREEP_INDEX_H
#define BTREEP_INDEX_H

/*

	BINDEX FILE STRUCTURE

	  13 bytes       8 bytes     key_len      8 bytes
	|---------------------------------------------------|
	|   bnode   |  interval[0] |  key[i]  | interval[i] ->
	|---------------------------------------------------|

*/

typedef struct BINDEX{

	//id of this node
	unsigned long id;

	//1 if leaf, 0 otherwise
	unsigned char is_leaf;

	//number of keys in this node
	unsigned int num_keys;

	//id of the parent
	unsigned long parent_file;

	//file pointer to this node in secondary memory
	FILE* file;

	void** keys;

	unsigned long* intervals;

}BINDEX;

//create file and return bindex struct
BINDEX* bindex_create(
			BTREEP*,
			unsigned long parent_file,
			unsigned int num_keys,
			void** keys,
			unsigned long* intervals
			);

//save bindex struct to file
void bindex_save(BTREEP*, BINDEX*);

//load bindex struct from file
BINDEX* bindex_load(BTREEP*, unsigned long id);

//free bindex struct, saving it before
void bindex_free(BTREEP*, BINDEX*);

/*
	POLYMORPHISM FUNCTIONS
*/

//return index of the element with the key given (return -1 on error)
unsigned int bindex_get_key_index(BTREEP*, BINDEX*, void* key);

//return key/data at given index (return NULL on error, like out of bounds and others)
//NOTE: return the actual pointer, not a copy
void* bindex_get_key_at(BTREEP*, BINDEX*, unsigned int index);

//set value of key at given index (return 1 if operation was sucessful)
unsigned char bindex_set_key_at(BTREEP*, BINDEX*, unsigned int index, void* key);

//remove key at given index and returns it (return NULL on error)
void* bindex_remove_key_at(BTREEP*, BINDEX*, unsigned int index);

//insert key in the right position, returns its index
unsigned int bindex_insert_key(BTREEP*, BINDEX*, void* key);

/*
	BINDEX SPECIFIC FUNCTIONS
*/

//NOTE: intervals should be added/removed after the key is added/removed

//get index of interval
unsigned int bindex_get_interval_index(BTREEP*, BINDEX*, unsigned long interval);

//return interval at index
unsigned long bindex_get_interval_at(BTREEP*, BINDEX*, unsigned int index);

//set interval at index (return 1 on sucess)
unsigned char bindex_set_interval_at(BTREEP*, BINDEX*, unsigned int index, unsigned long interval);

//insert new interval at index, return 1 on sucess, 0 otherwise
//NOTE: doesnt change bindex->num_keys
unsigned char bindex_insert_interval_at(BTREEP*, BINDEX*, unsigned int index, unsigned long interval);

//remove interval at index and return it
//NOTE: doesnt change bindex->num_keys
unsigned long bindex_remove_interval_at(BTREEP*, BINDEX*, unsigned int index);

//return right sibling, return 0 if none
unsigned long bindex_get_right_sibling(BTREEP*, BINDEX*);

//return left sibling, return 0 if none
unsigned long bindex_get_left_sibling(BTREEP*, BINDEX*);

//split y, returning new node
unsigned long bindex_split(BTREEP*, unsigned long y);

//steal key from sibling, return 1 if ok
unsigned char bindex_steal_from_sibling(BTREEP*, unsigned long id);

//merge with a sibling, return 1 if ok
unsigned char bindex_merge_with_sibling(BTREEP*, unsigned long id);

#endif
