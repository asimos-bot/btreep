#include "../btreep.h"

//print the whole tree
void btreep_print(BTREEP* btreep){

	if( !btreep || !btreep->root || !btreep->root->data ) return;

	btreep_print_bnode_rec(btreep, *((unsigned long*)btreep->root->data), 0);
}

//print bindex
void btreep_print_bnode_rec(BTREEP* btreep, unsigned long id, unsigned int offset){

	if( !btreep || !id || !bnode_get_num_keys(id) ) return;

	if( bnode_get_is_leaf(id) ){

		BLEAF* bleaf = bleaf_load(btreep, id);

		void* data;

		for(unsigned int i=0; i < offset; i++) printf(" ");
		printf("parent: %lu, id: %lu\n", bleaf->parent_file, bleaf->id);

		//iterate through data in leaf an print each one
		for(long i = bleaf->num_keys - 1; i >= 0; i--){

			data = bleaf_get_data_at(btreep, bleaf, (unsigned int)i);
			btreep->print_data(data, offset);
			btreep->free_data(data);
			printf("\n");

		}

		//free struct
		bleaf_free(btreep, bleaf);

		return;
	}

	BINDEX* bindex = bindex_load(btreep, id);

	//close the file for now, to avoid having a bunch of files open at the same time
	fclose(bindex->file);

	//print last interval
	btreep_print_bnode_rec(btreep, bindex->intervals[bindex->num_keys], offset+10);
		for(unsigned int i=0; i < offset; i++) printf(" ");
		printf("parent: %lu, id: %lu\n", bindex->parent_file, bindex->id);


	//iterate through keys and intervals
	for(long i = bindex->num_keys - 1; i >= 0; i--){

		btreep->print_key(bindex->keys[i], offset);
		printf("\n");
		btreep_print_bnode_rec(btreep, bindex->intervals[i], offset+10);
		printf("\n");
	}

	//reopen file
	char filename[30];
	sprintf(filename, "btreep_data/%u.dat", bindex->id);
	bindex->file = fopen(filename, "r+b");

	//free struct
	bindex_free(btreep, bindex);
}
