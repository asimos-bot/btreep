#ifndef BTREEP_PRINT_H
#define BTREEP_PRINT_H

//print the whole tree
void btreep_print(BTREEP*);

//print bindex
void btreep_print_bnode_rec(BTREEP*, unsigned long id, unsigned int offset);

#endif
