#ifndef BTREEP_STRUCT_H
#define BTREEP_STRUCT_H

typedef struct BTREEP{

	//ramification factor
	unsigned short t;

	//size of the keys (for indexes)
	unsigned int key_len;

	//size of the data (for leaves)
	unsigned int data_len;

	//function pointer for comparing keys (return 1 if a < b and 0 otherwise)
	unsigned char (*cmp)(void* a, void* b);

	//get key from data struct (return address of it in stack)
	void* (*key)(void* data);

	//print data
	void (*print_data)(void* data, unsigned int offset);

	//print key
	void (*print_key)(void* key, unsigned int offset);

	//free data (for leaves)
	void (*free_data)(void*);

	//free key (for bindexes)
	void (*free_key)(void*);

	//list with the id of each node in the tree (unsigned long)
	LIST* root;

}BTREEP;

//create btreep struct from given filename (if filename exists load existing btreep, otherwise create a new one)
//NOTE: if NULL is given a new btreep struct is created
//NOTE: t and key_len will not be used if filename exists
BTREEP* btreep_init(char* filename, unsigned short t, unsigned int key_len, unsigned int data_len);

//free btreep struct
void btreep_free(BTREEP*);

//save btreep struct in a file
void btreep_struct_save(BTREEP* btreep, char* filename);

//create new valid id and insert it into the list, also return the id
unsigned int btreep_struct_generate_id(BTREEP*);

//remove id from list, and also the id's file
void btreep_struct_remove_id(BTREEP*, unsigned long);

//change root node
void btreep_struct_change_root(BTREEP*, unsigned long);

#endif
