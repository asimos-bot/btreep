#include "../btreep.h"

//default functions for btreep struct
unsigned char default_cmp(int* a, int* b){

	return *a < *b;
}

void* default_key(void* a){

	return a;
}

void default_print_data(void* data, unsigned int offset){

	printf("(data here)");
}

void default_print_key(void* key, unsigned int offset){

	printf("(key here)");
}

//helper function, only available here
unsigned int btreep_struct_list_to_luint(LIST* list){

	return *((unsigned long*)list->data);
}

//helper funciton, only available here
BTREEP* btreep_struct_from_file(FILE* file){

	//if NULL is given, nothing happens
	if( !file ) return NULL;

	//go to the beginning
	fseek(file, 0, SEEK_SET);

	//allocate memory for btreep struct
	BTREEP* btreep = malloc(sizeof(BTREEP));

	btreep->cmp = (unsigned char(*)(void*, void*))default_cmp;
	btreep->key = default_key;
	btreep->print_data = default_print_data;
	btreep->print_key = default_print_key;
	btreep->free_data = free;
	btreep->free_key = free;
	btreep->root = NULL;

	//read ramification factor from file
	fread(&btreep->t, sizeof(unsigned short), 1, file);

	//read key_len from file
	fread(&btreep->key_len, sizeof(unsigned int), 1, file);

	//read data_len from file
	fread(&btreep->data_len, sizeof(unsigned int), 1, file);

	//read ids from file and add to btreep->root list
	unsigned long id;

	while( fread(&id, sizeof(unsigned long), 1, file) )
		btreep->root = list_insert(btreep->root, &id, sizeof(unsigned long));

	fclose(file);

	return btreep;
}

//create btreep struct from given filename (if filename exists load existing btreep, otherwise create a new one)
BTREEP* btreep_init(char* filename, unsigned short t, unsigned int key_len, unsigned int data_len){

	FILE* file;

	//load and return btreep struct if file exists
	if( file = fopen(filename, "rb") ) return btreep_struct_from_file(file);

	//else create new btreep struct if t and key_len are valid
	if( t<2 || !key_len || !data_len ) return NULL;

	BTREEP* btreep = malloc(sizeof(BTREEP));

	btreep->t = t;
	btreep->key_len = key_len;
	btreep->data_len = data_len;
	btreep->cmp = (unsigned char(*)(void*, void*))default_cmp;
	btreep->key = default_key;
	btreep->print_data = default_print_data;
	btreep->print_key = default_print_key;
	btreep->free_data = free;
	btreep->free_key = free;
	btreep->root = NULL;

	return btreep;
}

//free btreep struct
void btreep_free(BTREEP* btreep){

	if( !btreep ) return;

	list_free(btreep->root);

	free(btreep);
}

//save btreep struct in a file
void btreep_struct_save(BTREEP* btreep, char* filename){

	//if no filename or btreep struct is given,
	//or btreep has no ids, return NULL
	if( !btreep || !filename || !btreep->root ) return;

	FILE* file;

	//do nothing if file can't be opened
	if( !(file = fopen(filename, "wb")) ) return;

	fseek(file, 0, SEEK_SET);

	//write ramification factor to file
	fwrite(&btreep->t, sizeof(unsigned short), 1, file);

	//write key_len to file
	fwrite(&btreep->key_len, sizeof(unsigned int), 1, file);

	//write data_len to file
	fwrite(&btreep->data_len, sizeof(unsigned int), 1, file);

	//write ids to file (unsigned longs)
	for(LIST* list = btreep->root; list; list = list->next)
		fwrite(list->data, sizeof(unsigned long), 1, file);

	fclose(file);
}

//create new valid id and insert it into the list, also return the id
unsigned int btreep_struct_generate_id(BTREEP* btreep){

	//if nothing is given nothing happens
	if( !btreep ) return 0;

	//find id which is not in the list
	unsigned long i;

	for(i=1; list_search(btreep->root, &i, sizeof(unsigned long)); i++);
	
	//insert found valid id in list
	btreep->root = list_insert(btreep->root, &i, sizeof(unsigned long));

	return i;
}

//remove id from list, and also the id's file
void btreep_struct_remove_id(BTREEP* btreep, unsigned long id){

	//nothing happens if btreep or id is NULL
	if( !btreep || !id ) return;

	//remove id from list
	LIST* node = list_remove(btreep->root, &id, sizeof(unsigned long));

	//if id wasn't in list (or was root), stop function here
	if( !node ) return;
	//otherwise, free the node found
	list_free(node);

	//delete the file associated with the given id
	char filename[30];
	sprintf(filename, "btreep_data/%u.dat", id);
	remove(filename);
}

//change root node
void btreep_struct_change_root(BTREEP* btreep, unsigned long new_root){

	if( !btreep || !btreep->root || !new_root || new_root == *(unsigned long*)btreep->root->data ) return;

	//create new_root node
	LIST* new_node = list_create();
	list_alloc(new_node, sizeof(unsigned long));
	*((unsigned long*)new_node->data) = new_root;

	//remove new_root from list if it exists there
	list_free_node(list_remove(btreep->root, &new_root, sizeof(unsigned long)));

	//put new_root before current root
	new_node->next = btreep->root;
	btreep->root = new_node;
}
