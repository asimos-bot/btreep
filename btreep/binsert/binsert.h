#ifndef BTREEP_INSERT_H
#define BTREEP_INSERT_H

//main insert function
void btreep_insert(BTREEP*, void* data);

//update parent file field in the children of the given id
void btreep_update_childs(BTREEP*, unsigned long id);

//split node, return new node id
unsigned long btreep_insert_split(BTREEP*, unsigned long id);

#endif
