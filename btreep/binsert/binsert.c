#include "../btreep.h"

//main insert function
void btreep_insert(BTREEP* btreep, void* data){

	if( !btreep || !data ) return;

	if( !btreep->root || !btreep->root->data ){

		BLEAF* bleaf = bleaf_create(btreep, 0, 0, 0, 0);
		bleaf_insert_data(btreep, bleaf, data);
		bleaf_free(btreep, bleaf);

		return;
	}

	//get root of the tree
	unsigned long x, y;
	x = *((unsigned long*)btreep->root->data);

	//check if root is full
	if( !bnode_can_insert(x, btreep->t) ){

		btreep_insert_split(btreep, x);
		x = bnode_get_parent_file(x);
	}

	//while x is not leaf
	while( !bnode_get_is_leaf(x) ){

		//get next node to look in
		y = btreep_search_next_subtree_root(btreep, x, btreep->key(data));

		//check if we need to split it
		if( bnode_can_insert(y, btreep->t) ){

			//if not, just move along
			x = y;
		}else{

			//else, split and then move along

			//get middle key
			void* mid_key = bnode_get_key(btreep, y, btreep->t - 1);

			unsigned long new_y = btreep_insert_split(btreep, y);
	
			if( btreep->cmp(btreep->key(data), mid_key) )
				x = new_y;
			else
				x = y;

			btreep->free_key(mid_key);
		}
	}

	//insert it in x (which is now a leaf)

	//first get the struct
	BLEAF* bleaf = bleaf_load(btreep, x);

	//the actual insertion
	bleaf_insert_data(btreep, bleaf, data);
	
	//free struct and save struct
	bleaf_free(btreep, bleaf);
}

//update parent file field in the children of the given id
void btreep_update_childs(BTREEP* btreep, unsigned long id){

	if( !id ) return;

	BINDEX* bindex = bindex_load(btreep, id);

	for(unsigned int i = 0; i <= bindex->num_keys; i++)
		bnode_set_parent_file(bindex->intervals[i], bindex->id);

	bindex_free(btreep, bindex);
}

//split node, return new node id
unsigned long btreep_insert_split(BTREEP* btreep, unsigned long id){

	return bnode_get_is_leaf(id) ? bleaf_split(btreep, id) : bindex_split(btreep, id);
}
